
package com.screenshots;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;

public class GitlabScreenshotTest {
	private static ChromeDriver driver;

	@Before
	public void setUp() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\rjanagam\\Downloads\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void getGitlabScreenSHOT() throws Exception{
		driver.get("https://gitlab.com/users/sign_in");
		driver.manage().window().maximize();
		WebElement username = driver.findElement(By.id("user_login"));

		username.sendKeys("Rajahamsa");

		WebElement password = driver.findElement(By.id("user_password"));

		password.sendKeys("Vinayakaa45");

		driver.findElement(By.name("commit")).click();

		driver.findElement(By.cssSelector("a[href='/Rajahamsa/Graded_Lab2_Rajahamsa']")).click();
		
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		FileUtils.copyFile(scrFile, new File("C:\\Users\\rjanagam\\Downloads\\screenshotGitLab.png"));
		
		

		
		
	}
	
	@AfterClass
	public static void tearDown() {
		driver.close();
		driver.quit();
	}

}
