package com.screenshots;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class JenkinsScreenshotTest {
	private static ChromeDriver driver;

	@Before
	public void setUp() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\rjanagam\\Downloads\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void getJenkinsScreenSHOT() throws Exception{
		driver.get("localhost:8080");
		driver.manage().window().maximize();
		WebElement username = driver.findElement(By.id("j_username"));

		username.sendKeys("root");

		WebElement password = driver.findElement(By.name("j_password"));

		password.sendKeys("root");

		driver.findElement(By.id("yui-gen1-button")).click();

		driver.findElement(By.cssSelector("a[href='job/Graded_Lab2/']")).click();
		
		driver.findElement(By.cssSelector("a[href='/job/Graded_Lab2/build?delay=0sec']")).click();
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		FileUtils.copyFile(scrFile, new File("C:\\Users\\rjanagam\\Downloads\\screenshotJenkins1.png"));
		
		
		
		
	}


	@AfterClass
	public static void tearDown() {
		driver.close();
		driver.quit();
	}
}
