package com.retail.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ItemUPCPriceStepDefinationTest {
	private ChromeDriver driver;
	private Map<String, Double> intialiseItemUPCMap = new HashMap<String, Double>();
	List<String> UPC = new ArrayList<String>();
	List<Double> price = new ArrayList<Double>();
	Map<String, Double> outputMap = new HashMap<String, Double>();

	@Before("@Web")
	public void setUp() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\rjanagam\\Downloads\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Given("^the following  UPC and Weight:$")
	public void the_following_UPC_and_Weight(Map<String, Double> intialiseItemUPCMap) throws Throwable {
		this.intialiseItemUPCMap = intialiseItemUPCMap;
	}

	@When("^User Navigates to Create Shipment Page$")
	public void user_Navigates_to_Create_Shipment_Page() throws Throwable {
		driver.get("http://localhost:8085/SimpleRetailWeb/");
		WebElement element = driver.findElement(By.xpath("/html/body/form/input"));
		element.submit();
	}

	@Then("^UPC and Weight from the page should match the given UPC and Weight$")
	public void upc_and_Weight_from_the_page_should_match_the_given_UPC_and_Weight() throws Throwable {
		List<WebElement> UPCList = driver.findElements(By.xpath("/html/body/form/table/tbody/tr/td[2]"));

		for (int i = 0; i < UPCList.size(); i++) {
			UPC.add(UPCList.get(i).getText());
		}

		List<WebElement> priceList = driver.findElements(By.xpath("/html/body/form/table/tbody/tr/td[4]"));
		for (int i = 0; i < priceList.size(); i++) {
			price.add(Double.parseDouble(priceList.get(i).getText()));
		}

		for (int i = 0; i < UPCList.size(); i++) {

			outputMap.put(UPC.get(i), price.get(i));
		}

		assertTrue(outputMap.equals(intialiseItemUPCMap));

	}

	@After("@Web")
	public void tearDown() {
		driver.close();
		driver.quit();
	}

}
