package com.employee;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class IdStepDefinationTest {
	List<String> id = new ArrayList<String>();
	private ChromeDriver driver;

	@Before("@Web1")
	public void setUp() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\rjanagam\\Downloads\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Given("^the following Data:$")
	public void the_following_Data(List<String> id) throws Throwable {
		this.id = id;

	}

	@When("^User Navigates to ship Confirmation Page$")
	public void user_Navigates_to_ship_Confirmation_Page() throws Throwable {
		driver.get("http://localhost:8085/SimpleRetailWeb/");
		WebElement element = driver.findElement(By.xpath("/html/body/form/input"));
		element.submit();
		WebElement element1 = driver.findElement(By.xpath("//*[@id=\"checkedRows\"]"));
		element1.click();
		Select oSelect = new Select(driver.findElement(By.name("employee")));

		oSelect.selectByVisibleText("Raji,Janagama");

		WebElement element3 = driver.findElement(By.xpath("/html/body/form/input"));
		element3.click();

	}

	@Then("^Id from the page should match the given Id$")
	public void id_from_the_page_should_match_the_given_Id() throws Throwable {

		List<WebElement> idelements = driver.findElements(By.xpath("/html/body/p"));
		for (int i = 0; i < idelements.size(); i++) {
			Assert.assertEquals(id.get(i), idelements.get(i).getText());
		}
	}

	@After("@Web1")
	public void tearDown() {
		driver.close();
		driver.quit();
	}
}