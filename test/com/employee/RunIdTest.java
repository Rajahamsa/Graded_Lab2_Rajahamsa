package com.employee;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:target/Destination" }, features = "test/resources/Id.feature")
public class RunIdTest {

}
