package com.employee;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EmployeeHoursTest {

	EmployeeHours eh;
	EmployeeBean e;

	@Before
	public void setUp() throws Exception {
		eh = new EmployeeHours();
	}

	@Test
	public void testToValidateEmployeeHoursPositive() {
		e = new EmployeeBean("emp126", "sruthi", "Arugula", 5, 30);
		boolean canworkactual = eh.validateHours(e.getHours(), e.getMinutes());
		assertEquals(true, canworkactual);
	}

	@Test
	public void testToValidateEmployeeHoursNegative() {
		e = new EmployeeBean("emp126", "sruthi", "Arugula", 55, 30);
		boolean canworkactual = eh.validateHours(e.getHours(), e.getMinutes());
		assertEquals(false, canworkactual);

	}

}
