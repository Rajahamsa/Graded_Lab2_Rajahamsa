Feature: UPC and Price is valid on page
Ensure proper weights on are on shipment.jsp

@Web
Scenario: UPC and Weight validation
	Given the following  UPC and Weight:
	|567321101987|19.99|
	|567321101986|17.99|
	|567321101985|20.49|
	|567321101984|23.88|
	|467321101899|9.75 |
	|477321101878|17.25|
	
	When User Navigates to Create Shipment Page
	
	Then UPC and Weight from the page should match the given UPC and Weight
