package com.retail.web;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.employee.Employee;
import com.employee.EmployeeBean;
import com.retail.core.Item;
import com.retail.core.Products;

/**
 * Servlet implementation class ShippingServlet
 */
@WebServlet("/ShippingServlet")
public class ShippingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Item> myItems = Products.getItems();
		List<EmployeeBean> employees = Employee.getEmployee();

		request.getSession().setAttribute("items", myItems);
		request.getSession().setAttribute("employees", employees);

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/shipment.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String[] checkedIds = request.getParameterValues("checkedRows");
		List<Item> shipItems = (List<Item>) request.getSession().getAttribute("items");

		for (Iterator<Item> iter = (Iterator<Item>) shipItems.listIterator(); iter.hasNext();) {
			if (!(Arrays.asList(checkedIds).contains(iter.next().getUpc()))) {
				iter.remove();
			}

		}

		String name = request.getParameter("employee");
		List<EmployeeBean> employees = (List<EmployeeBean>) request.getSession().getAttribute("employees");
		String[] namesplit = name.split(",");

		for (Iterator<EmployeeBean> iter = (Iterator<EmployeeBean>) employees.listIterator(); iter.hasNext();) {
			if (!(namesplit[0].contains(iter.next().getFirstName()))) {
				iter.remove();
			}

		}

		request.getSession().setAttribute("shipItems", shipItems);
		request.getSession().setAttribute("employees", employees);

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/shipConfirmation.jsp");
		dispatcher.forward(request, response);
	}

}
