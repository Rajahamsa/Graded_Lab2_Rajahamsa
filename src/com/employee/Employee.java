package com.employee;

import java.util.ArrayList;
import java.util.List;

public class Employee {

	public static List<EmployeeBean> getEmployee() {

		EmployeeHours eh = new EmployeeHours();

		List<EmployeeBean> employees = new ArrayList<EmployeeBean>();
		List<EmployeeBean> validEmployees = new ArrayList<EmployeeBean>();

		employees.add(new EmployeeBean("emp123", "Raji", "Janagama", 35, 30));
		employees.add(new EmployeeBean("emp126", "sruthi", "Arugula", 5, 30));
		employees.add(new EmployeeBean("emp129", "sindhu", "G", 55, 30));
		employees.add(new EmployeeBean("emp129", "Harsha", "G", 25, 30));

		for (EmployeeBean e : employees) {
			boolean valid = eh.validateHours(e.getHours(), e.getMinutes());
			if (valid) {
				validEmployees.add(e);
			}
		}

		return validEmployees;

	}
}
