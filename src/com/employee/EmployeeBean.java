package com.employee;

public class EmployeeBean {
	String id;
	String firstName;
	String LastName;
	int hours;
	int minutes;

	public EmployeeBean() {
		super();
	}

	public EmployeeBean(String id, String firstName, String lastName, int hours, int minutes) {
		super();
		this.id = id;
		this.firstName = firstName;
		LastName = lastName;
		this.hours = hours;
		this.minutes = minutes;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	@Override
	public String toString() {
		return "EmployeeBean [id=" + id + ", firstName=" + firstName + ", LastName=" + LastName + ", hours=" + hours
				+ ", minutes=" + minutes + "]";
	}

}
