package com.employee;

public class EmployeeHours {

	public boolean validateHours(int hr, int min) {
		int totalMinutes = hr * 60 + min;
		boolean valid;
		if (totalMinutes > 2400) {
			valid = false;
		} else {
			valid = true;

		}

		return valid;
	}

}
